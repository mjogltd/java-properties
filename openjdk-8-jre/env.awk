BEGIN {FS = "=" }
        {
                v = $2;
                j=$2; for (i=3;i<=NF;i++) {
                    v = v "=" $i
                }
                FS="_"; $0 = $1;
                k=$2; for (i=3;i<=NF;i++) {
                        if (length($i) > 0) {
                                if (!skip) {
                                        k = k "." $i;
                                } else {
                                        k = k "_" $i;
                                }
                        } else { skip = 1; }
                }
                print k "=" v;
                FS = "=";
        }
